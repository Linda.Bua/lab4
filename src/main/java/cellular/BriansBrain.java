package cellular;
//import java.awt.Color;
import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
    }
        @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }
    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int x = 0; x < numberOfColumns(); x++ ){
            for (int y = 0; y < numberOfRows(); y++){
                CellState cellState = getNextCell(x, y);
                nextGeneration.set(x, y, cellState);
            }
        }
        currentGeneration = nextGeneration;
    }
    @Override
    public CellState getNextCell(int row, int col) {
        // IF ALIVE
        if (getCellState(row, col) == CellState.ALIVE) { 
				return CellState.DYING;
        }
        // IF DYING
		else if (getCellState(row, col) == CellState.DYING) { 
				return CellState.DEAD;
        }
        // IF DEAD
        else if (getCellState(row, col) == CellState.DEAD) {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }
    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    private int countNeighbors(int row, int col, CellState state) {
		int count = 0;

		for (int r = row - 1; r <= row + 1; r++) {
			for(int c = col - 1; c <= col + 1; c++) {
				if (r >= currentGeneration.numRows() || c >= currentGeneration.numColumns()) {
					continue;
				}
				if ((r == row) && (c == col)){
					continue;
				}
				else if (state == currentGeneration.get(r, c)) {
					count++;
				}
			}
		}
		return count;
	}

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    } 
}
