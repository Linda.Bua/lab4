package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    
    private int rows; // Hight
    private int cols; // Width
    private CellState[][] grid; //Datastructure

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        grid = new CellState[rows][columns];
        for (int x = 0; x < numRows(); x++) {
            for (int y = 0; y < numColumns(); y++) {
                grid[x][y] = initialState;
            }
        }
	}
    @Override
    public int numRows() {
        return rows;
    }
    @Override
    public int numColumns() {
        return cols;
    }
    @Override
    public void set(int row, int column, CellState element) {
           if ((row >= 0) && (row < numRows())) {
               if ((column >= 0) && (column < numColumns()));
               grid[row][column] = element;
            } 
            else {
                throw new IndexOutOfBoundsException();
            }
        }
    @Override
    public CellState get(int row, int column) {
        CellState theState = grid[row][column];
        if ((row >= 0) && (row < numRows())) {
            if ((column >= 0) && (column < numColumns()));
                return theState; 
            } 
            else {
                throw new IndexOutOfBoundsException();
            }
        }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(rows,cols,CellState.ALIVE);
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++){
                newGrid.grid[x][y] = this.grid[x][y];
            }  
        }return newGrid;
    }
}
